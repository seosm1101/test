package co.yedam.book.service.impl;

import java.util.List;

import co.yedam.book.dao.BookDAO;
import co.yedam.book.service.BookService;
import co.yedam.book.vo.BookVO;

public class BookServiceImpl implements BookService {
	
	BookDAO dao=BookDAO.getInstance();

	@Override
	public int addBook(BookVO vo) {
		
		return dao.addBook(vo);
	}

	@Override
	public List<BookVO> bookList() {

		return dao.bookList();
	}

	@Override
	public BookVO searchBook(String bookCode) {

		return dao.searchBook(bookCode);
	}

	@Override
	public int bookDelete(String bookCode) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}
