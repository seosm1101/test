package co.yedam.book.service.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import co.yedam.book.mapper.BookMapper;
import co.yedam.book.service.BookService;
import co.yedam.book.vo.BookVO;
import co.yedam.common.DataSource;

public class BookServiceImplMybatis implements BookService {
	
	SqlSession session = DataSource.getInstance().openSession(true);
	BookMapper mapper = session.getMapper(BookMapper.class);
	
	@Override
	public int addBook(BookVO vo) {
		
		return mapper.addBook(vo);
	}

	@Override
	public List<BookVO> bookList() {
		
		return mapper.bookList();
	}

	@Override
	public BookVO searchBook(String bookCode) {

		return mapper.searchBook(bookCode);
	}

	@Override
	public int bookDelete(String bookCode) {

		return mapper.bookDelete(bookCode);
	}

}
