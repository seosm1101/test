package co.yedam.book.service;

import java.util.List;

import co.yedam.book.vo.BookVO;

public interface BookService {
	
	//도서 등록
	public int addBook(BookVO vo);
	//도서 목록
	public List<BookVO> bookList();
	//도서 상세조회
	public BookVO searchBook(String bookCode);
	//도서 삭제
	public int bookDelete(String bookCode);
}
