package co.yedam.book.mapper;

import java.util.List;

import co.yedam.book.vo.BookVO;

public interface BookMapper {
	public List<BookVO> bookList();
	public int addBook(BookVO vo);
	public BookVO searchBook(String bookCode);
	public int bookDelete(String bookCode);
}
