package co.yedam.book.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;

public class bookInfoAjaxForm implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {

		return "book/bookInfoAjax.tiles";
	}

}
