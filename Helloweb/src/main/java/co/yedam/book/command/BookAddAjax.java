package co.yedam.book.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.book.service.BookService;
import co.yedam.book.service.impl.BookServiceImplMybatis;
import co.yedam.book.vo.BookVO;
import co.yedam.common.Command;

public class BookAddAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		String bookCode = req.getParameter("bookCode");
		String bookTitle = req.getParameter("bookTitle");
		String bookAuthor = req.getParameter("bookAuthor");
		String bookPress = req.getParameter("bookPress");
		int bookPrice = Integer.parseInt(req.getParameter("bookPrice"));
		
		BookVO vo = new BookVO();
		vo.setBookCode(bookCode);
		vo.setBookTitle(bookTitle);
		vo.setBookAuthor(bookAuthor);
		vo.setBookPress(bookPress);
		vo.setBookPrice(bookPrice);
		
		BookService service = new BookServiceImplMybatis();
		
		int r=service.addBook(vo);
		String json="";
		if(r>0) {
			json="{\"retCode\": \"Success\"}";
		} else {
			json="{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
	}

}
