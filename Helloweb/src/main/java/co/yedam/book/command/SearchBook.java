package co.yedam.book.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.book.service.BookService;
import co.yedam.book.service.impl.BookServiceImplMybatis;
import co.yedam.book.vo.BookVO;
import co.yedam.common.Command;

public class SearchBook implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		String bookCode=req.getParameter("bookCode");
		
		BookService service = new BookServiceImplMybatis();
		
		BookVO vo = service.searchBook(bookCode);
		
		req.setAttribute("searchBook", vo);
		
		return "book/searchBook.tiles";
	}

}
