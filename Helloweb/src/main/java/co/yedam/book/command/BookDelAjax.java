package co.yedam.book.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.book.service.BookService;
import co.yedam.book.service.impl.BookServiceImplMybatis;
import co.yedam.common.Command;

public class BookDelAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {

		String bookCode = req.getParameter("bookCode");
		
		BookService service = new BookServiceImplMybatis();
		
		int r = service.bookDelete(bookCode);
		String json ="";
		if(r>0) {
			json = "{\"retCode\": \"Success\"}";
		} else {
			json = "{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
	}

}
