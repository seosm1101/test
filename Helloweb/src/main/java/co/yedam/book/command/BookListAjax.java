package co.yedam.book.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.yedam.book.service.BookService;
import co.yedam.book.service.impl.BookServiceImplMybatis;
import co.yedam.book.vo.BookVO;
import co.yedam.common.Command;


public class BookListAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {

		BookService service = new BookServiceImplMybatis();
		List<BookVO> list = service.bookList();
		
		//jackson 라이브러리 활용.
		ObjectMapper mapper = new ObjectMapper();
		try {
			String json = mapper.writeValueAsString(list);
			return json + ".ajax";
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
