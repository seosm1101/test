package co.yedam.book.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.book.service.BookService;
import co.yedam.book.service.impl.BookServiceImplMybatis;
import co.yedam.book.vo.BookVO;
import co.yedam.common.Command;

public class Bokklist implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		
		BookService service = new BookServiceImplMybatis();
		List<BookVO> list = service.bookList();
		
		req.setAttribute("list", list);
		
		return "book/bookList.tiles";
	}

}
