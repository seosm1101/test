package co.yedam.book.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.yedam.book.vo.BookVO;
import co.yedam.common.DAO;

public class BookDAO {
	
	Connection conn;
	ResultSet rs;
	PreparedStatement psmt;
	
	private static BookDAO instance = new BookDAO();
	
	private BookDAO() {
		
	}
	
	public static BookDAO getInstance() {
		return instance;
	}
	
	public void close() {
		try {
			if (conn != null)
				conn.close();
			if (rs != null)
				rs.close();
			if (psmt != null)
				psmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//도서 상세조회
	public BookVO searchBook(String bookCode) {
		BookVO vo = new BookVO();
		String sql="select * from tbl_book where book_code=?";
		conn=DAO.getConn();
		try {
			psmt=conn.prepareStatement(sql);
			psmt.setString(1, bookCode);
			rs=psmt.executeQuery();
			if(rs.next()) {
				vo.setBookCode(rs.getString("book_code"));
				vo.setBookAuthor(rs.getString("book_author"));
				vo.setBookTitle(rs.getString("book_title"));
				vo.setBookPress(rs.getString("book_press"));
				vo.setBookPrice(rs.getInt("book_price"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return vo;
	}
	
	//도서 목록
	public List<BookVO> bookList(){
		List<BookVO> list = new ArrayList<>();
		String sql="select * from tbl_book";
		conn=DAO.getConn();
		try {
			psmt=conn.prepareStatement(sql);
			rs=psmt.executeQuery();
			while(rs.next()) {
				BookVO vo = new BookVO();
				vo.setBookCode(rs.getString("book_code"));
				vo.setBookAuthor(rs.getString("book_author"));
				vo.setBookTitle(rs.getString("book_title"));
				vo.setBookPress(rs.getString("book_press"));
				vo.setBookPrice(rs.getInt("book_price"));
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return list;
	}
	
	//도서 등록
	public int addBook(BookVO vo) {
		String sql="insert into tbl_book\r\n"
				+ "values(?, ?, ?, ?, ?)";
		conn=DAO.getConn();
		try {
			psmt=conn.prepareStatement(sql);
			psmt.setString(1, vo.getBookCode());
			psmt.setString(2, vo.getBookTitle());
			psmt.setString(3, vo.getBookAuthor());
			psmt.setString(4, vo.getBookPress());
			psmt.setInt(5, vo.getBookPrice());
			int r=psmt.executeUpdate();
			return r;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return 0;
	}
}
