package co.yedam;

import java.util.ArrayList;
import java.util.List;

import co.yedam.notice.service.impl.NoticeServiceImplMybatis;
import co.yedam.notice.vo.NoticeVO;

public class MainApp {
	public static void main(String[] args) {

		List<NoticeVO>list =new ArrayList<>();
		NoticeVO v1 = new NoticeVO();
		v1.setNoticeId(100);
		NoticeVO v2 = new NoticeVO();
		v2.setNoticeId(200);
		NoticeVO v3 = new NoticeVO();
		v3.setNoticeId(300);
		
		list.add(v1);
		list.add(v2);
		list.add(v3);
		
		NoticeServiceImplMybatis mybatis = new NoticeServiceImplMybatis();
		for(NoticeVO notice : mybatis.forEachTest(list)) {
			System.out.println(notice);
		}
	}
}
