package co.yedam.web;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.book.command.AddBook;
import co.yedam.book.command.AddBookForm;
import co.yedam.book.command.Bokklist;
import co.yedam.book.command.BookAddAjax;
import co.yedam.book.command.BookDelAjax;
import co.yedam.book.command.BookListAjax;
import co.yedam.book.command.SearchBook;
import co.yedam.book.command.bookInfoAjaxForm;
import co.yedam.common.AddCenterInfo;
import co.yedam.common.AddCenterJson;
import co.yedam.common.Command;
import co.yedam.common.MainDo;
import co.yedam.member.command.AddMember;
import co.yedam.member.command.AjaxForm;
import co.yedam.member.command.InsertMember;
import co.yedam.member.command.LoginCheck;
import co.yedam.member.command.LoginForm;
import co.yedam.member.command.Logout;
import co.yedam.member.command.MemberAddAjax;
import co.yedam.member.command.MemberDeleteAjax;
import co.yedam.member.command.MemberList;
import co.yedam.member.command.MemberListAjax;
import co.yedam.member.command.MemberListAjaxJackson;
import co.yedam.member.command.MemberModAjax;
import co.yedam.notice.command.AddEventJson;
import co.yedam.notice.command.AllEventJson;
import co.yedam.notice.command.ChartDo;
import co.yedam.notice.command.DeleteEventJson;
import co.yedam.notice.command.DeleteNotice;
import co.yedam.notice.command.FullCalendarDo;
import co.yedam.notice.command.NoticeList;
import co.yedam.notice.command.ProductList;
import co.yedam.notice.command.RelatedList;
import co.yedam.notice.command.SearchNotice;
import co.yedam.notice.command.SearchProduct;
import co.yedam.notice.command.UpdateNotice;
import co.yedam.notice.command.addNotice;
import co.yedam.notice.command.insertNotice;

@WebServlet("*.do")
public class FrontController extends HttpServlet {
	
	HashMap<String, Command>map;
	
	public FrontController() {
		map=new HashMap<>();
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		//notice
		map.put("/main.do", new MainDo());
		map.put("/noticeList.do", new NoticeList());
		map.put("/searchNotice.do", new SearchNotice());
		map.put("/updateNotice.do", new UpdateNotice());
		map.put("/addNotice.do", new addNotice());
		map.put("/insertNotice.do", new insertNotice());
		map.put("/deleteNotice.do", new DeleteNotice());
		//회원관련.
		map.put("/loginForm.do", new LoginForm());
		map.put("/loginCheck.do", new LoginCheck());
		map.put("/logOut.do", new Logout());
		map.put("/addMemberForm.do", new AddMember());
		map.put("/inserMember.do", new InsertMember());
		map.put("/memberList.do", new MemberList());
		//도서관련.
		map.put("/addBookForm.do", new AddBookForm());
		map.put("/addBook.do", new AddBook());
		map.put("/bookList.do", new Bokklist());
		map.put("/searchBook.do", new SearchBook());
		//ajax관련.
		map.put("/ajaxMember.do", new AjaxForm());
		map.put("/memberListAjax.do", new MemberListAjax());
		map.put("/memberListAjaxJackson.do", new MemberListAjaxJackson());
		map.put("/deleteMemberAjax.do", new MemberDeleteAjax());
		map.put("/addMemberAjax.do", new MemberAddAjax());
		map.put("/modMemberAjax.do", new MemberModAjax());
		//chart관련.
		map.put("/chart.do", new ChartDo());
		//여러건 데이터를 centerInfo에 등록
		map.put("/addCenterInfo.do", new AddCenterInfo());	//284번 개별적으로 호출.
		map.put("/addCenterJson.do", new AddCenterJson());
		//이벤트 정보.
		map.put("/allEventjson.do", new AllEventJson());
		map.put("/addEvent.do", new AddEventJson());
		map.put("/deleteEvent.do", new DeleteEventJson());
		map.put("/fullCalendar.do", new FullCalendarDo());
		//상품
		map.put("/productList.do", new ProductList());
		map.put("/searchProduct.do", new SearchProduct());
		map.put("/relatedList.do", new RelatedList());
		//ajaxBook
		map.put("/bookInfoAjaxForm.do", new bookInfoAjaxForm());
		map.put("/bookListAjax.do", new BookListAjax());
		map.put("/bookDelAjax.do", new BookDelAjax());
		map.put("/bookAddAjax.do", new BookAddAjax());
		
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		req.setCharacterEncoding("utf-8");
		String uri=req.getRequestURI(); //Helloweb/hello.do
		String context=req.getContextPath(); //Helloweb
		String page=uri.substring(context.length()); //hello.do
		System.out.println("==>"+page);
		
		Command command=map.get(page);
		String viewPage=command.exec(req, resp); //dir/file.jsp
		
		if(viewPage.endsWith(".do")) {
			resp.sendRedirect(viewPage);
		}else if(viewPage.endsWith(".tiles")) {
			RequestDispatcher rd=req.getRequestDispatcher(viewPage);
			rd.forward(req, resp);
		}else if(viewPage.endsWith(".ajax")) {	//{"id":"hond","age":20}.ajax
			resp.setContentType("text/json;charset=utf-8");
			resp.getWriter().print(viewPage.substring(0, viewPage.length()-5));
		}
		
	}
}
