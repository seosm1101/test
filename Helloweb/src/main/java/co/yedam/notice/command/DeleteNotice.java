package co.yedam.notice.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;

public class DeleteNotice implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		//파리미터(삭제할글번호) 읽어와서
		int noticeId=Integer.parseInt(req.getParameter("num"));
		//db
		NoticeService service=new NoticeServiceImplMybatis();
		service.deleteNotice(noticeId);
		
		return "noticeList.do";
	}

}
