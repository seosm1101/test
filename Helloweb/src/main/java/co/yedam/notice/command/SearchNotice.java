package co.yedam.notice.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;
import co.yedam.notice.vo.NoticeVO;

public class SearchNotice implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		String num=req.getParameter("num");
		//페이지정보
		String pageNum=req.getParameter("pageNum");
		String searchCondition=req.getParameter("searchCondition");
		String keyword=req.getParameter("keyword");
		
		NoticeService service=new NoticeServiceImplMybatis();
		NoticeVO vo=service.searchNotice(Integer.parseInt(num));
		
		req.setAttribute("vo", vo);
		
		req.setAttribute("pageNum", pageNum);
		req.setAttribute("searchCondition", searchCondition);
		req.setAttribute("keyword", keyword);
		
		return "notice/searchNotice.tiles";
	}

}
