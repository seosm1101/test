package co.yedam.notice.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.common.Criteria;
import co.yedam.common.PageDTO;
import co.yedam.common.SearchVO;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImpl;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;
import co.yedam.notice.vo.NoticeVO;

public class UpdateNotice implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {

//		req.setAttribute("pageNum", pageNum);
//		req.setAttribute("searchCondition", searchCondition);
//		req.setAttribute("keyword", keyword);
		
		//페이지정보
		String nid=req.getParameter("num");
		String title=req.getParameter("title");
		String subject=req.getParameter("subject");
		
		
		NoticeVO vo=new NoticeVO();
		vo.setNoticeId(Integer.parseInt(nid));
		vo.setNoticeTitle(title);
		vo.setNoticeSubject(subject);
		
		//update 구현
		NoticeService service=new NoticeServiceImplMybatis();
		service.updateNotice(vo);
		
		//데이터베이스 수정 구문..요기까지
		
		//NoticeList.java
		String searchCondition=req.getParameter("searchCondition");
		String keyword=req.getParameter("keyword");
		String pageNum=req.getParameter("pageNum");

		SearchVO svo=new SearchVO();
		svo.setSearchCondition(searchCondition);
		svo.setKeyword(keyword);
		
		pageNum = pageNum == null ? "1" : pageNum;
		int pageNumInt=Integer.parseInt(pageNum);
		
		NoticeService dao=new NoticeServiceImpl();
		int total=dao.noticeListPagingTotalCnt(svo);
		
		List<NoticeVO>list=dao.noticeListPaging(pageNumInt, svo);
		req.setAttribute("noticeList", list);
		
		Criteria cri=new Criteria(pageNumInt,10);
		PageDTO dto=new PageDTO(cri, total);
		req.setAttribute("pageDTO", dto);
		req.setAttribute("searchvo", svo);
		
		return "notice/noticeList.tiles";
	}

}
