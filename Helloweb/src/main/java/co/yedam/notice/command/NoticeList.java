package co.yedam.notice.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.common.Criteria;
import co.yedam.common.PageDTO;
import co.yedam.common.SearchVO;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;
import co.yedam.notice.vo.NoticeVO;

public class NoticeList implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String searchCondition=req.getParameter("searchCondition");
		System.out.println(searchCondition);
		String keyword=req.getParameter("keyword");
		SearchVO svo=new SearchVO();
		svo.setSearchCondition(searchCondition);
		svo.setKeyword(keyword);
		
		String pageNum=req.getParameter("pageNum");
		pageNum = pageNum == null ? "1" : pageNum;
		
		int pageNumInt=Integer.parseInt(pageNum);
		
		
		
		NoticeService dao=new NoticeServiceImplMybatis();
		int total=dao.noticeListPagingTotalCnt(svo);
		
		List<NoticeVO>list=dao.noticeListPaging(pageNumInt, svo);
		
		req.setAttribute("noticeList", list);
		
		Criteria cri=new Criteria(pageNumInt,10);
		PageDTO dto=new PageDTO(cri, total);
		req.setAttribute("pageDTO", dto);
		
		req.setAttribute("searchvo", svo);
		//web-inf 폴더안에 있을경우 직접접근불가
		return "notice/noticeList.tiles";
	}

}
