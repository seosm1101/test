package co.yedam.notice.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import co.yedam.common.Criteria;
import co.yedam.common.DataSource;
import co.yedam.common.SearchVO;
import co.yedam.notice.mapper.NoticeMapper;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.vo.NoticeVO;

public class NoticeServiceImplMybatis implements NoticeService {
	
	SqlSession session = DataSource.getInstance().openSession(true);
	NoticeMapper mapper = session.getMapper(NoticeMapper.class);
	
	//foreach 연습
	public List<NoticeVO> forEachTest(List<NoticeVO>list){
		return mapper.forEachTest(list);
	}
	
	public int addCenterInfo(Map<String, Object>map) {
		return mapper.addCenterInfo(map);
	}
	
	public int insertCentereInfo(List<Map<String, Object>> list) {
		return mapper.insertCentereInfo(list);
	}
	
	@Override
	public List<NoticeVO> noticeList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NoticeVO> noticeListPaging(int pageNum, SearchVO svo) {
		//mybatis parameter 로 넘기기 위해 Criterial 활용
		Criteria cri = new Criteria(pageNum, 10);
		cri.setSearchCondition(svo.getSearchCondition());
		cri.setKeyword(svo.getKeyword());
		
		return mapper.noticeListPaging(cri);
	}

	@Override
	public int noticeListPagingTotalCnt(SearchVO svo) {
		return mapper.noticeListPagingTotalCnt(svo);
	}

	@Override
	public NoticeVO searchNotice(int noticeId) {
		mapper.addHitCount(noticeId);
		return mapper.searchNotice(noticeId);
	}

	@Override
	public int updateNotice(NoticeVO vo) {
		return mapper.updateNotice(vo);
	}

	@Override
	public int insertNotice(NoticeVO vo) {
		return mapper.insertNotice(vo);
	}

	@Override
	public int deleteNotice(int noticeId) {
		return mapper.deleteNotice(noticeId);
	}

	@Override
	public Map<String, Integer> chartData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> allEvents() {
		return mapper.allEvents();
	}

	@Override
	public int addEvent(Map<String, Object> map) {

		return mapper.addEvent(map);
	}

	@Override
	public int deleteEvent(String title) {

		return mapper.deleteEvent(title);
	}
	
	public List<Map<String, Object>> productList(){
		
		return mapper.productList();
	}

	@Override
	public List<Map<String, Object>> productSearch(String prodCode) {

		return mapper.productSearch(prodCode);
	}

	@Override
	public List<Map<String, Object>> relatedList() {

		return mapper.relatedList();
	}
	
}
