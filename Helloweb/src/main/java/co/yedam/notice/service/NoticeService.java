package co.yedam.notice.service;

import java.util.List;
import java.util.Map;

import co.yedam.common.SearchVO;
import co.yedam.notice.vo.NoticeVO;

public interface NoticeService {
	
	public List<NoticeVO>noticeList();
	//page에 따른 목록.
	public List<NoticeVO>noticeListPaging(int pageNum, SearchVO svo);
	//page목록 계산을 위한 건수.
	public int noticeListPagingTotalCnt(SearchVO svo);
	//단건조회 글번호->글전체정보반환
	public NoticeVO searchNotice(int noticeId);
	//글정보 수정
	public int updateNotice(NoticeVO vo);
	//글추가
	public int insertNotice(NoticeVO vo);
	//글삭제
	public int deleteNotice(int noticeId);
	//차트용 데이터
	public Map<String, Integer> chartData();
	//이벤트 데이터
	public List<Map<String, Object>> allEvents();
	//이벤트 등록
	public int addEvent(Map<String, Object> map);
	//이벤트 삭제
	public int deleteEvent(String title);
	//
	public List<Map<String, Object>> productList();
	//
	public List<Map<String, Object>> productSearch(String prodCode);
	//
	public List<Map<String,Object>> relatedList();
}
