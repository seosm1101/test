package co.yedam.notice.service.impl;

import java.util.List;
import java.util.Map;

import co.yedam.common.SearchVO;
import co.yedam.notice.dao.NoticeDAO;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.vo.NoticeVO;

public class NoticeServiceImpl implements NoticeService {
	
	//JDBC 사용 처리.
	NoticeDAO dao=NoticeDAO.getInstance();
	
	@Override
	public List<NoticeVO> noticeList() {
		
		return dao.noticeList();
	}

	@Override
	public List<NoticeVO> noticeListPaging(int pageNum, SearchVO svo) {

		return dao.noticeListPaging(pageNum,svo);
	}

	@Override
	public int noticeListPagingTotalCnt(SearchVO svo) {

		return dao.noticeListTotalCnt(svo);
	}

	@Override
	public NoticeVO searchNotice(int noticeId) {
		dao.addHitCount(noticeId);
		return dao.searachNotice(noticeId);
	}

	@Override
	public int updateNotice(NoticeVO vo) {
		
		return dao.updateNotice(vo);
	}

	@Override
	public int insertNotice(NoticeVO vo) {
		
		return dao.insertNotice(vo);
	}

	@Override
	public int deleteNotice(int noticeId) {
		
		return dao.deleteNotice(noticeId);
	}

	@Override
	public Map<String, Integer> chartData() {

		return dao.chartData();
	}

	@Override
	public List<Map<String, Object>> allEvents() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int addEvent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteEvent(String title) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Map<String, Object>> productList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> productSearch(String prodCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> relatedList() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
