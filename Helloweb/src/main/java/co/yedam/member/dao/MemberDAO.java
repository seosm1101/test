package co.yedam.member.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.yedam.common.DAO;
import co.yedam.member.vo.MemberVO;

public class MemberDAO {
	
	Connection conn;
	ResultSet rs;
	PreparedStatement psmt;
	
	private static MemberDAO instance=new MemberDAO();
	
	private MemberDAO() {
		
	}
	
	public static MemberDAO getInstance() {
		return instance;
	}
	
	public void close() {
		try {
			if (conn != null)
				conn.close();
			if (rs != null)
				rs.close();
			if (psmt != null)
				psmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//회원겁색
	public MemberVO searchMember(String id) {
		MemberVO vo = new MemberVO();
		String sql="select * from member where member_id=?";
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, id);
			rs=psmt.executeQuery();
			
			if(rs.next()) {
				vo.setMemberId(rs.getString("member_id"));
				vo.setMemberName(rs.getString("member_name"));
				vo.setMemberPhone(rs.getString("member_phone"));
				vo.setMemberAddr(rs.getString("member_addr"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return vo;
	}
		
	//회원정보수정.
	public int updateMember(MemberVO vo) {
		String sql="update member set member_addr=?, member_phone=? where member_id=?";
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, vo.getMemberAddr());
			psmt.setString(2, vo.getMemberPhone());
			psmt.setString(3, vo.getMemberId());
			
			return psmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return 0;
	}
	
	//회원삭제
	public int deleteMember(String id) {
		String sql="delete from member where member_id=?";
		conn=DAO.getConn();
		int r = 0;
		try {
			psmt=conn.prepareStatement(sql);
			psmt.setString(1, id);
			r = psmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return r;
	}
	
	//회원목록 보기
	public List<MemberVO> memberList(){
		List<MemberVO>list=new ArrayList<>();
		String sql="select * from member";
		conn=DAO.getConn();
		try {
			psmt=conn.prepareStatement(sql);
			rs=psmt.executeQuery();
			while(rs.next()) {
				MemberVO vo=new MemberVO();
				vo.setMemberId(rs.getString("member_id"));
				vo.setMemberName(rs.getString("member_name"));
				vo.setMemberPhone(rs.getString("member_phone"));
				vo.setMemberAddr(rs.getString("member_addr"));
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return list;
	}
	
	//회원정보 등록
	public int insertMember(MemberVO vo) {
		String sql="insert into member\r\n"
				+ "values(?, ?, ?, ?, ?, null, ?)";
		conn=DAO.getConn();
		try {
			psmt=conn.prepareStatement(sql);
			psmt.setString(1, vo.getMemberId());
			psmt.setString(2, vo.getMemberPw());
			psmt.setString(3, vo.getMemberPhone());
			psmt.setString(4, vo.getMemberAddr());
			psmt.setString(5, vo.getMemberGrade());
			psmt.setString(6, vo.getMemberName());
			int r=psmt.executeUpdate();
			return r;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return 0;
	}
	
	//회원정보 등록
		public int addMember(MemberVO vo) {
			String sql="insert into member\r\n"
					+ "values(?, ?, ?, ?, 'User', null, ?)";
			conn=DAO.getConn();
			try {
				psmt=conn.prepareStatement(sql);
				psmt.setString(1, vo.getMemberId());
				psmt.setString(2, vo.getMemberPw());
				psmt.setString(3, vo.getMemberPhone());
				psmt.setString(4, vo.getMemberAddr());
				psmt.setString(5, vo.getMemberName());
				int r=psmt.executeUpdate();
				return r;
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close();
			}
			return 0;
		}
	
	//로그인
	public MemberVO loginCheck(String id, String pw) {
		String sql="select * from member where member_id=? and member_pw=?";
		conn=DAO.getConn();
		try {
			psmt=conn.prepareStatement(sql);
			psmt.setString(1, id);
			psmt.setString(2, pw);
			rs=psmt.executeQuery();
			
			if(rs.next()) {
				MemberVO vo=new MemberVO();
				vo.setMemberId(rs.getString("member_id"));
				vo.setMemberPw(rs.getString("member_pw"));
				vo.setMemberAddr(rs.getString("member_addr"));
				vo.setMemberPhone(rs.getString("member_phone"));
				vo.setMemberGrade(rs.getString("member_grade"));
				vo.setMemberName(rs.getString("member_name"));
				return vo;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
		return null;
	}
}
