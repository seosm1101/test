package co.yedam.member.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import co.yedam.common.Command;

public class Logout implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		HttpSession session=req.getSession();
		session.invalidate(); //세션값삭제
		return "member/loginForm.tiles";
	}

}
