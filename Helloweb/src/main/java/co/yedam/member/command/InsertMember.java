package co.yedam.member.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.member.service.MemberService;
import co.yedam.member.service.impl.MemberServiceImpl;
import co.yedam.member.vo.MemberVO;

public class InsertMember implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		String memberId=req.getParameter("memberId");
		String memberPw=req.getParameter("memberPw");
		String memberName=req.getParameter("memberName");
		String memberPhone=req.getParameter("memberPhone");
		String memberAddr=req.getParameter("memberAddr");
		String memberGrade=req.getParameter("memberGrade");
		
		MemberVO vo=new MemberVO();
		
		vo.setMemberId(memberId);
		vo.setMemberPw(memberPw);
		vo.setMemberName(memberName);
		vo.setMemberPhone(memberPhone);
		vo.setMemberAddr(memberAddr);
		if(memberGrade==null) {
			vo.setMemberGrade("User");
		} else {
			vo.setMemberGrade(memberGrade);
		}
		MemberService service=new MemberServiceImpl();
		service.insertMember(vo);
		
		return "main.do";
	}

}
