package co.yedam.member.command;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.yedam.common.Command;
import co.yedam.member.service.MemberService;
import co.yedam.member.service.impl.MemberServiceImplMybatis;
import co.yedam.member.vo.MemberVO;

public class MemberModAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		String id=req.getParameter("id");
		String addr=req.getParameter("addr");
		String phone=req.getParameter("phone");
		
		MemberVO vo=new MemberVO();
		vo.setMemberId(id);
		vo.setMemberAddr(addr);
		vo.setMemberPhone(phone);
		
		MemberService service=new MemberServiceImplMybatis();
		Map<String, Object> resultMap=new HashMap<>();
		
		if(service.updateMember(vo)>0) {
			vo=service.searchMember(id);
			
			resultMap.put("retCode", "Success");
			resultMap.put("memberInfo", vo);
		} else {
			resultMap.put("retCode", "Fail");
		}
	
		//json포맷생성
		String json="";
		ObjectMapper mapper=new ObjectMapper();
		try {
			json=mapper.writeValueAsString(resultMap);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return json + ".ajax";
	}

}
