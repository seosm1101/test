package co.yedam.member.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.member.service.MemberService;
import co.yedam.member.service.impl.MemberServiceImplMybatis;
import co.yedam.member.vo.MemberVO;

public class MemberAddAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		String id=req.getParameter("id");
		String pw=req.getParameter("pw");
		String phone=req.getParameter("phone");
		String addr=req.getParameter("addr");
		String name=req.getParameter("name");
		
		MemberVO vo=new MemberVO();
		vo.setMemberId(id);
		vo.setMemberPw(pw);
		vo.setMemberPhone(phone);
		vo.setMemberAddr(addr);
		vo.setMemberName(name);
		
		MemberService service=new MemberServiceImplMybatis();
		int r=service.addMember(vo);
		String json="";
		if(r>0) {
			json="{\"retCode\": \"Success\"}";
		} else {
			json="{\"retCode\": \"Fail\"}";
		}
		return json + ".ajax";
	}

}
