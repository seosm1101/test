package co.yedam.member.service;

import java.util.List;

import co.yedam.member.vo.MemberVO;

public interface MemberService {
	
	//로그인
	public MemberVO loginCheck(String id, String pw);
	//회원정보 등록
	public int insertMember(MemberVO vo);
	//회원목록 조회
	public List<MemberVO> memberList();
	//회원삭제
	public int deleteMember(String id);
	//회원등록
	public int addMember(MemberVO vo);
	//회원수정
	public int updateMember(MemberVO vo);
	//회원검색
	public MemberVO searchMember(String id);
}
