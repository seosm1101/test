package co.yedam.member.service.impl;

import java.util.List;

import co.yedam.member.dao.MemberDAO;
import co.yedam.member.service.MemberService;
import co.yedam.member.vo.MemberVO;

public class MemberServiceImpl implements MemberService {
	
	MemberDAO dao=MemberDAO.getInstance();
	
	@Override
	public MemberVO loginCheck(String id, String pw) {
		
		return dao.loginCheck(id,pw);
	}

	@Override
	public int insertMember(MemberVO vo) {
		
		return dao.insertMember(vo);
	}

	@Override
	public List<MemberVO> memberList() {

		return dao.memberList();
	}

	@Override
	public int deleteMember(String id) {
		
		return dao.deleteMember(id);
	}

	@Override
	public int addMember(MemberVO vo) {

		return dao.addMember(vo);
	}

	@Override
	public int updateMember(MemberVO vo) {

		return dao.updateMember(vo);
	}

	@Override
	public MemberVO searchMember(String id) {
		
		return dao.searchMember(id);
	}

}
