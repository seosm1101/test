package co.yedam.member.service.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import co.yedam.common.DataSource;
import co.yedam.member.service.MemberService;
import co.yedam.member.vo.MemberVO;

public class MemberServiceImplMybatis implements MemberService{
	//mybatis 활용
	//DataSource.getInstance() => SqlSessionFactory 반환.
	SqlSession session = DataSource.getInstance().openSession(true);
	
	
	@Override
	public MemberVO loginCheck(String id, String pw) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int insertMember(MemberVO vo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<MemberVO> memberList() {
		return session.selectList("co.yedam.member.mapper.MemberMapper.memberList");
	}

	@Override
	public int deleteMember(String id) {
		return session.delete("co.yedam.member.mapper.MemberMapper.deleteMember",id);
	}

	@Override
	public int addMember(MemberVO vo) {
		return session.insert("co.yedam.member.mapper.MemberMapper.addMember", vo);
	}

	@Override
	public int updateMember(MemberVO vo) {
		return session.update("co.yedam.member.mapper.MemberMapper.updateMember", vo);
	}

	@Override
	public MemberVO searchMember(String id) {
		return session.selectOne("co.yedam.member.mapper.MemberMapper.searchMember", id);
	}
	
}
