package co.yedam.common;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.JSONParser;

import co.yedam.notice.service.impl.NoticeServiceImplMybatis;

public class AddCenterJson implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		int result=0;
		try {
			ServletInputStream sis=req.getInputStream();	//byte의 형태
//			System.out.println(sis);
			byte[] bytes=sis.readAllBytes();
//			System.out.println(bytes);
			String json = new String(bytes);	//String json="test";
//			System.out.println(json);
			
			//json=>object :JSON.parse
			JSONParser parser = new JSONParser();
//			System.out.println(parser);
			Object obj = parser.parse(json);
//			System.out.println(obj);
			
			//[{},{},{},{},...]
			ArrayList<Map<String, Object>>list = (ArrayList<Map<String, Object>>) obj;
			for(Map<String, Object> map : list) {
				Set<String> keys = map.keySet();
				for(String key : keys) {
					System.out.println(key+" : "+map.get(key));
				}
			}
			
			//메소드 호출
			NoticeServiceImplMybatis ibatis = new NoticeServiceImplMybatis();
			result = ibatis.insertCentereInfo(list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "{retCode: " + result+ "}.ajax";
	}

}
