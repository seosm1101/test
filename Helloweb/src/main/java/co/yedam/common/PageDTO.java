package co.yedam.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PageDTO {
	private int startPage;
	private int endPage;
	private boolean prev, next;
	private int total;
	
	//현재페이지 12, 한페이지당 건수
	private Criteria cri;
	
	public PageDTO(Criteria cri, int total) {
		this.cri=cri;
		this.total=total;
		//endpage=현재 12page=>20page Math.ceil(12    /     10.0           ) * 10 =>20page
		this.endPage=(int)(Math.ceil(cri.getPageNum()/(cri.getAmount()*1.0)))*cri.getAmount();
		this.startPage=this.endPage -(cri.getAmount()-1);
		int realEnd=(int)(Math.ceil((total*1.0)/cri.getAmount()));
		
		if(this.endPage>realEnd) {
			this.endPage=realEnd;
		}
		
		this.prev=this.startPage>1;
		this.next=this.endPage<realEnd;
	}
	
}
