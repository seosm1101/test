/**
 * ajaxForm.jsp
 */

//헤더 타이틀.
let titles=['아이디','이름','연락처','주소','삭제'];
let columns=['memberId','memberName','memberPhone','memberAddr'];

//member값을 던져주면 tr생성해주는 함수
function makeTr(member={}){
	tr=document.createElement('tr');
	for(let column of columns){
		th=document.createElement('td');
		th.innerText=member[column];
		tr.append(th);
	}
	
	//삭제버튼
	th=document.createElement('td');
	let btn=document.createElement('button');
	btn.addEventListener('click', memberDeleteAjax)
	btn.innerText="삭제";
	th.append(btn) // <td><button>삭제</button></td>
	tr.append(th);
	
	//수정버튼
	th=document.createElement('td');
	btn=document.createElement('button');
	btn.addEventListener('click', memberModifyForm)
	btn.innerText='수정';
	th.append(btn); // <td><button>수정</button></td>
	tr.append(th);
	return tr;
}

//member리스트
const xhtp = new XMLHttpRequest();
xhtp.open('get', 'memberListAjaxJackson.do');
xhtp.send();
xhtp.onload = function(){
	//parse : json문자열-> javascrip object
	console.log(JSON.parse(xhtp.response));
	let result = JSON.parse(xhtp.response);
	
	
	//table 생성
	let tbl = document.createElement('table');
	let thd = document.createElement('thead');
	let tbd = document.createElement('tbody');
	let tr = document.createElement('tr');
	
	// thead의 타이틀 지정
	for(let title of titles){
		let th = document.createElement('th');
		th.innerText = title;
		tr.append(th);
	}
	thd.append(tr); // <thead><tr>...</tr></thead>
	
	//tbody의 값을 지정.
	for(let i=0; i<result.length; i++){
		let tr=makeTr(result[i]);
		tbd.append(tr);
	}
	
	//table의 하위에 thead, tbody위치
	tbl.append(thd, tbd);
	console.log(tbl);
	//div요소의 하위에 만든 table 지정
	tbl.setAttribute('border','1');
	tbl.className='table';
	document.getElementById('show').append(tbl);
}

//삭제버튼 클릭시 실행 이벤트
function memberDeleteAjax(){
	console.log(this); //이벤트를 만드는 대상 button
	console.log(this.parentElement.parentElement.firstChild.innerText);
	let user=this.parentElement.parentElement.firstChild.innerText;
	let btn=this;
	// db삭제. 화면에서 제거.
	const xhtp=new XMLHttpRequest();
	xhtp.open('get','deleteMemberAjax.do?id='+user);
	xhtp.send();
	xhtp.onload=function(){
		console.log(xhtp.response);	//retCode=Success, retCode=Fail
		let result=JSON.parse(xhtp.response);
		if (result.retCode == 'Success'){
			alert('정상적으로 처리완료.');
			btn.parentElement.parentElement.remove();
		} else if (result.retCode == 'Fail'){
			alert('처리중 오류가 발생.');
		}
	}
}

//등록번호 클릭 이벤트
document.getElementById('addBtn').addEventListener('click', addMemberAjax);

//등록버튼 Ajax호출
function addMemberAjax(){
	let id=document.getElementById('mid').value;
	let name=document.getElementById('mname').value;
	let addr=document.getElementById('maddr').value;
	let phone=document.getElementById('mphone').value;
	let pass=document.getElementById('pass').value;
	
	const member={
		memberId: id,
		memberName: name,
		memberAddr: addr,
		memberPhone: phone,
		memberPw: pass
	}
	
	const addAjax=new XMLHttpRequest();
	addAjax.open('post', 'addMemberAjax.do');	//전송정보를 body에 담아서 전송
	addAjax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	addAjax.send('id=' + id + '&name=' + name + '&addr=' + addr + '&phone=' + phone + '&pass=' + pass);
	addAjax.onload=function(){
		console.log(addAjax.response);
		let result=JSON.parse(addAjax.response);
		if(result.retCode == 'Success'){
			alert('정상처리 완료');
			document.querySelector('#show tbody').append(makeTr(member));
		} else if(result.retCode == 'Fail') {
			alert('처리중 에러');
		}
	}
}

//수정버튼 클릭시 실행 이벤트
function memberModifyForm(){
	let tr=this.parentElement.parentElement;
	let mid=tr.children[0].textContent;
	let mname=tr.children[1].textContent;
	let mphone=tr.children[2].textContent;
	let maddr=tr.children[3].textContent;
	
	const member={
		memberId: mid,
		memberName: mname,
		memberAddr: maddr,
		memberPhone: mphone
	}
	
	let newTr=document.createElement('tr');
	for (let column of columns){
		let td=document.createElement('td');
		if(column == 'memberAddr' || column == 'memberPhone'){
			let inp=document.createElement('input');
			inp.value=member[column];
			td.append(inp);
		} else {
			td.innerText=member[column];
		}
	newTr.append(td);
	}
	
	//버튼등록.
	//삭제버튼.
	td=document.createElement('td');
	let btn=document.createElement('button');
	btn.addEventListener('click', memberDeleteAjax)
	btn.innerText='삭제';
	btn.disabled=true;	//비활성화
	td.append(btn); // <td><button>삭제</button></td>
	newTr.append(td);
	
	//수정버튼.
	td=document.createElement('td');
	btn=document.createElement('button');
	btn.addEventListener('click', memberUpdateAjax)
	btn.innerText='변경';
	td.append(btn); // <td><button>변경</button></td>
	newTr.append(td);
	
	//tr요소를 newTr요소로 대체.
	tr.replaceWith(newTr);
}

function memberUpdateAjax(){
	let tr=this.parentElement.parentElement;
	let id=tr.children[0].textContent;
	let phone=tr.children[2].firstChild.value;
	let addr=tr.children[3].firstChild.value;

	const modAjax=new XMLHttpRequest();
	modAjax.open('post', 'modMemberAjax.do'); // 전송정보를 body에 담아서 전송
	modAjax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
	modAjax.send('id=' + id + '&addr=' + addr + '&phone=' + phone);
	modAjax.onload=function(){
	let result=JSON.parse(modAjax.response)
	console.log(result);
			
		if (result.retCode == 'Success') {
			alert('정상 처리 완료.');
			let newTr = makeTr(result.memberInfo);
			tr.replaceWith(newTr);
		} else if (result.retCode == 'Fail') {
			alert('처리중 에러.');
		}
	}
}
