/**
 * js/ajaxFetch.js
 */

//헤더 타이틀.
let titles=['아이디','이름','연락처','주소','삭제'];
let columns=['memberId','memberName','memberPhone','memberAddr'];

//member값을 던져주면 tr생성해주는 함수
function makeTr(member={}){
	tr=document.createElement('tr');
	for(let column of columns){
		th=document.createElement('td');
		th.innerText=member[column];
		tr.append(th);
	}
	
	//삭제버튼
	th=document.createElement('td');
	let btn=document.createElement('button');
	btn.addEventListener('click', memberDeleteAjax)
	btn.innerText="삭제";
	th.append(btn) // <td><button>삭제</button></td>
	tr.append(th);
	
	//수정버튼
	th=document.createElement('td');
	btn=document.createElement('button');
	btn.addEventListener('click', memberModifyForm)
	btn.innerText='수정';
	th.append(btn); // <td><button>수정</button></td>
	tr.append(th);
	return tr;
}

function showMemberList(result){
				
	console.log(result);
	
	//table 생성
	let tbl = document.createElement('table');
	let thd = document.createElement('thead');
	let tbd = document.createElement('tbody');
	let tr = document.createElement('tr');
	
	// thead의 타이틀 지정
	for(let title of titles){
		let th = document.createElement('th');
		th.innerText = title;
		tr.append(th);
	}
	thd.append(tr); // <thead><tr>...</tr></thead>
	
	result.forEach(function(item, idx, ary){
		console.log(item);
		tbd.append(makeTr(item));
	})
	
	//table의 하위에 thead, tbody위치
	tbl.append(thd, tbd);
	console.log(tbl);
	//div요소의 하위에 만든 table 지정
	tbl.setAttribute('border','1');
	tbl.className='table';
	document.getElementById('show').append(tbl);
}

function addMemberFnc(){
	let id=document.getElementById('mid').value;
	let name=document.getElementById('mname').value;
	let addr=document.getElementById('maddr').value;
	let phone=document.getElementById('mphone').value;
	let pass=document.getElementById('pass').value;
	
	const obj={
		memberId: id,
		memberName: name,
		memberAddr: addr,
		memberPhone: phone,
		memberPw: pass
	};
	const member='id=' + id + '&name=' + name + '&addr=' + addr + '&phone=' + phone + '&pass=' + pass;
	
	// post요청 content-type
	fetch("addMemberAjax.do",{
		method: 'post',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		body: member
	})
	.then(resolve => resolve.json())
	.then(result => {
		if(result.retCode=='Success'){
			document.querySelector('#show tbody').append(makeTr(obj));
		}else if(result.retCode=='Fail'){
			alert('처리 중 에러.')
		}
	})
	.catch(reject => {
		console.log(reject)
	})
	
}

function memberDeleteAjax(){
	let user=this.parentElement.parentElement.firstChild.innerText;
	let btn=this;
	fetch("deleteMemberAjax.do?id="+user,{
		method: 'Delete',
	})
	.then(resolve => resolve.json())
	.then(result => {
		if (result.retCode == 'Success'){
			alert('정상적으로 처리완료.');
			//console.log(btn.parentElement.parentElement);
			btn.parentElement.parentElement.remove();
		} else if (result.retCode == 'Fail'){
			alert('처리중 오류가 발생.');
		}
	})
	.catch(reject => {
		console.log(reject);
	})

}

function memberModifyForm(){
	let tr=this.parentElement.parentElement;
	let mid=tr.children[0].textContent;
	let mname=tr.children[1].textContent;
	let mphone=tr.children[2].textContent;
	let maddr=tr.children[3].textContent;
	
	const member={
		memberId: mid,
		memberName: mname,
		memberAddr: maddr,
		memberPhone: mphone
	}
	
	let newTr=document.createElement('tr');
	for (let column of columns){
		let td=document.createElement('td');
		if(column == 'memberAddr' || column == 'memberPhone'){
			let inp=document.createElement('input');
			inp.value=member[column];
			td.append(inp);
		} else {
			td.innerText=member[column];
		}
	newTr.append(td);
	}
	
	//버튼등록.
	//삭제버튼.
	td=document.createElement('td');
	let btn=document.createElement('button');
	btn.addEventListener('click', memberDeleteAjax)
	btn.innerText='삭제';
	btn.disabled=true;	//비활성화
	td.append(btn); // <td><button>삭제</button></td>
	newTr.append(td);
	
	//수정버튼.
	td=document.createElement('td');
	btn=document.createElement('button');
	btn.addEventListener('click', memberUpdateAjax)
	btn.innerText='변경';
	td.append(btn); // <td><button>변경</button></td>
	newTr.append(td);
	
	//tr요소를 newTr요소로 대체.
	tr.replaceWith(newTr);
}

function memberUpdateAjax(){
	let tr=this.parentElement.parentElement;
	let id=tr.children[0].textContent;
	let phone=tr.children[2].firstChild.value;
	let addr=tr.children[3].firstChild.value;
	const member='id=' + id + '&addr=' + addr + '&phone=' + phone;
	
	fetch("modMemberAjax.do",{
		method: 'post',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		body: member
	})
	.then(resolve => resolve.json())
	.then(result => {
		if (result.retCode == 'Success'){
			alert('정상 처리 완료.');
			let newTr = makeTr(result.memberInfo);
			tr.replaceWith(newTr);
		} else if (result.retCode == 'Fail'){
			alert('처리중 오류가 발생.');
		}
	})
	.catch(reject => {
		console.log(reject);
	})
}


//목록출력. fetch api를 활용
//fetch('url').then(처리완료시실행함수).catch(오류시처리함수)
fetch ('memberListAjaxJackson.do')
.then(resolve => resolve.json())	//json stream을 javascript object생성
.then(showMemberList)
.catch(function(reject){
	console.log(reject)
})


document.getElementById('addBtn').addEventListener('click', addMemberFnc);
