/**
 * 
 */


//헤더 타이틀
let titles=['도서코드','제목','저자','출판사','가격', '삭제'];
let columns=['bookCode','bookTitle','bookAuthor','bookPress','bookPrice'];

//전체체크
function allCheckFnc(){
	let allChk=document.querySelectorAll('input[type="checkbox"]');
    for(let chk of allChk){
        chk.checked=this.checked;
    }
}

//tr생성
function makeTr(book={}){
	tr=document.createElement('tr');
	
	td=document.createElement('td');
	let check = document.createElement('input');
    check.setAttribute('type', 'checkbox');
	td.append(check);
	tr.append(td);
	
	for(let column of columns){
		td=document.createElement('td');
		td.innerText=book[column];
		tr.append(td);
	}
	
	//삭제버튼
	td=document.createElement('td');
	let btn=document.createElement('button');
	btn.addEventListener('click', deleteBookFnc);
	btn.innerText="삭제";
	td.append(btn);
	tr.append(td);
	
	return tr;
}

//삭제버튼
function deleteBookFnc(){
	let bookCode=this.parentElement.parentElement.childNodes[1].innerText;
	console.log(this);
	console.log(bookCode);
	let btn=this;
	fetch("bookDelAjax.do?bookCode="+bookCode,{
		method: 'Delete',
	})
	.then(resolve => resolve.json())
	.then(result => {
		if (result.retCode == 'Success'){
			alert('정상적으로 처리완료.');
			btn.parentElement.parentElement.remove();
		} else if (result.retCode == 'Fail'){
			alert('처리중 오류가 발생.');
		}
	})
	.catch(reject => {
		console.log(reject);
	})
}

//목록
fetch('bookListAjax.do')
.then(resolve => resolve.json())
.then(bookList)
.catch(reject => console.log(reject))

//목록출력
function bookList(result){
	console.log(result);
	let tbl = document.createElement('table');
	let thd = document.createElement('thead');
	let tbd = document.createElement('tbody');
	let tr = document.createElement('tr');
	
	//thead
	th = document.createElement('th');
	let check = document.createElement('input');
    check.setAttribute('type', 'checkbox');
	check.addEventListener('click', allCheckFnc);
	th.append(check);
	tr.append(th);
	
	for(let title of titles){
		let th = document.createElement('th');
		th.innerText = title;
		tr.append(th);
	}
	thd.append(tr);
	
	result.forEach(function(item, idx, ary){
		console.log(item);
		tbd.append(makeTr(item));
	})
	
	tbl.append(thd, tbd);
	tbl.setAttribute('border', '1');
	tbl.className='table';
	document.getElementById('show').append(tbl);
}

document.getElementById('addBtn').addEventListener('click', addBookFnc)
document.getElementById('delBtn').addEventListener('click', checkDelBookFnc)

//선택삭제
function checkDelBookFnc(){
	let allChk=document.querySelectorAll('tbody input[type="checkbox"]:checked');
	console.log(allChk);
	
	for (let chk of allChk) {
		console.log(chk);
		let bookCode=chk.parentElement.parentElement.childNodes[1].innerText;
		console.log(bookCode);
		fetch("bookDelAjax.do?bookCode="+bookCode,{
			method: 'Delete',
		})
		.then(resolve => resolve.json())
		.then(result => {
			if (result.retCode == 'Success'){
				chk.parentElement.parentElement.remove();
			} else if (result.retCode == 'Fail'){
				alert('처리중 오류가 발생.');
			}
		})
		.catch(reject => {
			console.log(reject);
		})
	}
}

//추가
function addBookFnc(){
	let bookCode=document.getElementById('fbookCode').value;
	let bookTitle=document.getElementById('fbookTitle').value;
	let bookAuthor=document.getElementById('fbookAuthor').value;
	let bookPress=document.getElementById('fbookPress').value;
	let bookPrice=document.getElementById('fbookPrice').value;
	
	const obj={
		bookCode: bookCode,
		bookTitle: bookTitle,
		bookAuthor: bookAuthor,
		bookPress: bookPress,
		bookPrice: bookPrice
	};
	
	fetch("bookAddAjax.do", {
		method: 'post',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		body: 'bookCode=' + bookCode + '&bookTitle=' + bookTitle + '&bookAuthor=' + bookAuthor + '&bookPress=' + bookPress + '&bookPrice=' + bookPrice
	})
	.then(resolve => resolve.json())
	.then(result => {
		if(result.retCode=='Success'){
			alert('정상적으로 처리완료.');
			document.querySelector('#show tbody').append(makeTr(obj));
		}else if(result.retCode=='Fail'){
			alert('처리중 오류가 발생.');
		}
	})
	.catch(reject => console.log(reject))
}