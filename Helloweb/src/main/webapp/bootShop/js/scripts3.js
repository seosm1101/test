/**
 * 
 */

let url=location.href;
let pos=url.indexOf('prodCode=');
let prodCode=url.substring(59);

fetch('../searchProduct.do', {
	method: 'post',
	headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	body: 'prodCode=' + prodCode 
})
.then(resolve => resolve.json())
.then(result => {
	console.log(result);
	console.log(result[0]);
	console.log(result[0].IMAGE);
	
	let session = document.querySelector('.py-5');
	console.log(session);
	session.querySelector('img').setAttribute('src','../images/'+result[0].IMAGE);
	
	session.querySelector('h1').innerText = result[0].PROD_NAME;
	
	session.querySelector('span:nth-of-type(1)').innerText = result[0].ORIG_PRICE;
	session.querySelector('span:nth-of-type(2)').innerText = result[0].SALE_PRICE;
	
	session.querySelector('p').innerText = result[0].PROD_DESC;
})
.catch(reject => console.log(reject))


fetch('../relatedList.do')
.then(resolve => resolve.json())
.then(result => {
	console.log(result);

	for(let i=0; i<result.length;i++){
		let template = document.querySelector('.col.mb-5').cloneNode(true);
	
		//console.log(template.querySelector('h5'));		
		//상품이름
		template.querySelector('h5').innerText=result[i].PROD_NAME;
		
		//이미지
		template.querySelector('img').setAttribute('src','../images/'+result[i].IMAGE)
		//원가격..할인가격.
		template.querySelector('span:nth-of-type(1)').innerText=result[i].ORIG_PRICE;
		template.querySelector('span:nth-of-type(2)').innerText=result[i].SALE_PRICE;
		//document.getElementById('spanOne').innerText=result[i].origPrice;
		//document.getElementById('spanTwo').innerText=result[i].salePrice;
		//평점.
		let sItem = template.querySelector('div.d-flex');
		for(let j=0; j<5; j++){
			let star =document.createElement('div');
			console.log(result[i].likeIt);
			
			if(j<Math.floor(result[i].likeIt)){
				star.className = 'bi-star-fill'; //온별
			} else if(j<result[i].likeIt){
				star.className = 'bi-star-half'; //반별
			} else {
				star.className= 'bi-star'; //공별
			}
			sItem.children[j].replaceWith(star)
		}
		document.querySelector('.col.mb-5').parentElement.append(template);
	}
	//template으로 사용한 요소는 화면에서 클리어.
	document.querySelector('.col.mb-5').style.display='none';
	
})
.catch(reject => console.log(reject))
