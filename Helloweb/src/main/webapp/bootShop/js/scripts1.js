/**
 * 
 */
          
fetch('../productList.do')
.then(resolve => resolve.json())
.then(result => {
	console.log(result);

	for(let i=0; i<result.length;i++){
		let template = document.querySelector('.col.mb-5').cloneNode(true);
	
		//console.log(template.querySelector('h5'));		
		//상품이름
		template.querySelector('h5').innerText=result[i].prodName;
		//링크
		let atag=document.createElement('a');
		atag.setAttribute('href','item.html?prodCode='+result[i].prodCode);
		atag.setAttribute('target','_blank');
		//이미지경로
		let itag=template.querySelector('img').cloneNode(true);
		itag.setAttribute('src','../images/'+result[i].image);
		atag.append(itag);
		template.querySelector('img').replaceWith(atag);
		
		//ahref.setAttribute('href','item.html?prodCode='+result[i].prodCode);
		//ahref.setAttribute('target', '_blank');
		//원가격..할인가격.
		template.querySelector('span:nth-of-type(1)').innerText=result[i].origPrice;
		template.querySelector('span:nth-of-type(2)').innerText=result[i].salePrice;
		//document.getElementById('spanOne').innerText=result[i].origPrice;
		//document.getElementById('spanTwo').innerText=result[i].salePrice;
		//평점.
		let sItem = template.querySelector('div.d-flex');
		for(let j=0; j<5; j++){
			let star =document.createElement('div');
			console.log(result[i].likeIt);
			
			if(j<Math.floor(result[i].likeIt)){
				star.className = 'bi-star-fill'; //온별
			} else if(j<result[i].likeIt){
				star.className = 'bi-star-half'; //반별
			} else {
				star.className= 'bi-star'; //공별
			}
			sItem.children[j].replaceWith(star)
		}
		document.querySelector('.col.mb-5').parentElement.append(template);
	}
	//template으로 사용한 요소는 화면에서 클리어.
	document.querySelector('.col.mb-5').style.display='none';
	
})
.catch(reject => console.log(reject))

//let template = document.querySelector('.col.mb-5'); //div.col.m5-5
//let parentEl = template.parentElement;	//div.row.gx-4-lg-5...

/*
for(let i=0; i<5; i++){
	template = document.querySelector('.col.mb-5').cloneNode(true);
	
	parentEl.append(template);
}
*/