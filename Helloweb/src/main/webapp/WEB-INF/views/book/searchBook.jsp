<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<h3>상세화면(bookDetail.jsp)</h3>
<form action="updateBook.do">
	<input type="hidden" name="bookCode" value="${vo.bookCode }">
	<table class="table">
		<tr>
			<th style="width:400px">도서코드</th>
			<td><input style="width:500px" type="text" value=${searchBook.bookCode }><td>
		</tr>
		<tr>
			<th>제목</th>
			<td><input style="width:500px" type="text" value=${searchBook.bookTitle }><td>
		</tr>
		<tr>
			<th>저자</th>
			<td><input style="width:500px" type="text" value=${searchBook.bookAuthor }><td>
		</tr>
		<tr>
			<th>출판사</th>
			<td><input style="width:500px" type="text" value=${searchBook.bookPress }><td>
		</tr>
		<tr>
			<th>가격</th>
			<td><input style="width:500px" type="text" value=${searchBook.bookPrice }><td>
		</tr>
	</table>
	<div style="text-align: center;margin-right: 150px ">
		<input class="btn btn-primary" type="submit" value="수정">
		<input class="btn btn-primary" type="button" value="삭제">
	</div>
</form>