<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<h3>도서등록화면(addBookForm.jsp)</h3>
<form action="addBook.do" method="post">
	<table class="table">
		<tr>
			<td style="width:400px">도서코드</td>
			<td><input style="width:500px" type="text" name="bookCode"></td>
		</tr>
		<tr>
			<td>제목</td>
			<td><input style="width:500px" type="text" name="bookTitle"></td>
		</tr>
		<tr>
			<td>저자</td>
			<td><input style="width:500px" type="text" name="bookAuthor"></td>
		</tr>
		<tr>
			<td>출판사</td>
			<td><input style="width:500px" type="text" name="bookPress"></td>
		</tr>
		<tr>
			<td>가격</td>
			<td><input style="width:500px" type="text" name="bookPrice"></td>
		</tr>	
	</table>
	<div style="text-align: center;margin-right: 150px ">
		<input class="btn btn-primary" type="submit" value="등록">
		<input class="btn btn-primary" type="reset" value="취소">
	</div>	
</form>