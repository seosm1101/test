<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h3>공지사항등록</h3>
<form action="insertNotice.do" method="post">
<table class="table">
	<c:choose>
	<c:when test="${not empty id }">
		<tr>
			<th>작성자</th>
			<td><input type="text" name="writer" value=${id } readonly></td>
		</tr>
	</c:when>
	<c:otherwise>
		<tr>
			<th>작성자</th>
			<td><input type="text" name="writer"></td>
		</tr>
	</c:otherwise>
	</c:choose>
	<tr>
		<th>제목</th>
		<td><input type="text" name="title"></td>
	</tr>
	<tr>
		<th>내용</th>
		<td><textarea name="subject" cols=30 rows=5></textarea></td>
	</tr>
	<c:choose>
		<c:when test="${not empty id }">
		<tr>
			<td><input type="submit" value="등록"></td>
		</tr>
		</c:when>
		<c:otherwise>
		<tr>
			<td><input disabled type="submit" value="등록"></td>
		</tr>
		</c:otherwise>
	</c:choose>
</table>
</form>

