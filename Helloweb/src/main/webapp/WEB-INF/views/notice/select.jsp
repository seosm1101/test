<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />
<link href='css/main.css' rel='stylesheet' />
<script src='js/main.js'></script>
<script>
	let allEvents=[]
	
   
  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
	// start
		//추가. 비동기방식처리.
		fetch('allEventjson.do')	//allEvenJson.do vs fullcal/example/select.html
		.then(resolve => resolve.json())
		.then(result => {
			console.log(result)
			console.log(allEvents)
			
			result.forEach(event => {
				let obj = {title: event.title, start: event.startDate, end: event.endDate}
				allEvents.push(obj);	//배열에 등록
			})
			//호출
			  var calendar = new FullCalendar.Calendar(calendarEl, {
			      headerToolbar: {
			        left: 'prev,next today',
			        center: 'title',
			        right: 'dayGridMonth,timeGridWeek,timeGridDay'
			      },
			      initialDate: new Date(),
			      navLinks: true, // can click day/week names to navigate views
			      selectable: true,
			      selectMirror: true,
			      select: function(arg) {
			    	  console.log(arg);
			        var title = prompt('일정을 입력하세요!');
			        //db입력처리.
			        //정상처리 화면출력
			        //에러발생 에러발생, 화면추가x
			        if (title) {
				        fetch('addEvent.do', {
				        	method: 'post',
				        	headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				        	body: 'title=' + title + '&startDate=' + arg.startStr + '&endDate=' + arg.endStr
				        })
				        .then(resolve => resolve.json())
				        .then(resolve => {
				        	console.log(resolve)
				        	if(resolve.retCode=='Success'){
					          //화면에 일정 추가
					          calendar.addEvent({
					            title: title,
					            start: arg.start,
					            end: arg.end,
					            allDay: arg.allDay
					          })
					          //화면일정추가
				        	} else {
								alert('이벤트 등록 에러.');				        		
				        	}
				        	calendar.unselect()
				        })
				        .catch(reject => console.log(reject))
			        }	//end of if(title)
			       
			      },
			      eventClick: function(arg) {
			        if (confirm('Are you sure you want to delete this event?')) {
			        	//db 반영.
			        	//정상처리 => 화면에서 제거
			        	//처리오류 => 오류발생 메세지
			        	console.log(arg)
			        	console.log(arg.event._def.title)
			        	title=arg.event._def.title;
			        	fetch('deleteEvent.do', {
			        		method: 'post',
			        		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			        		body: 'title=' + title
			        	})
			        	.then(resolve => resolve.json())
			        	.then(resolve => {
				        	console.log(resolve)
				        	if(resolve.retCode=='Success'){
				        		arg.event.remove()
				        	} else {
								alert('이벤트 등록 에러.');				        		
				        	}
				        	
				        })
			            
			        }
			      },
			      editable: true,
			      dayMaxEvents: true, // allow "more" link when too many events
			      events: allEvents
				});
			    
			    calendar.render();	//calendar 화면에 출력.
		})
		.catch(reject => console.error(reject));
	//end

  });
</script>
<style>
body {
	margin: 40px 10px;
	padding: 0;
	font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
	font-size: 14px;
}

#calendar {
	max-width: 1100px;
	margin: 0 auto;
}
</style>
</head>
<body>

	<div id='calendar'></div>

</body>
</html>
