<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<h3>첫페이지</h3>

<button id="addBtn">등록</button>

<script>
const url='https://api.odcloud.kr/api/15077586/v1/centers?page=1&perPage=284&returnType=JSON&serviceKey=zgxIx6swsyGkB9hbVUj2Arv%2FWZSmwS1T0O%2BMO4GD9j%2FJmPzto9Bo7TkjZoN2eJtJDYnwIMEE6mrQsvOrvZ3eZw%3D%3D';
function testFnc(){
	fetch(url)
	.then(resolve => resolve.json())
	.then(result => {
		console.log(result);
		result.data.forEach(center => {
			addCenterInfoFnc(center);
		})
	})
	.catch(err => console.log(err))
}

document.getElementById('addBtn').addEventListener('click', addCenterInfoFnc2)

function addCenterInfoFnc2(){
	fetch(url)
	.then(resolve => resolve.json())
	.then(result => {
		console.log(result);
		console.log(result.data);
		let tData=result.data;
		transferToCenter(tData); //한번에 처리해주기 위해서
	})
	.catch(err => console.log(err))
}

function transferToCenter(args){
	//json=>object : JSON.parse()
	//object=>json : Json.stringify()
	let jsonStr=JSON.stringify(args);
	fetch('addCenterJson.do',{
		method: 'post',
		headers: {'Content-Type': 'application/json'},
		body: jsonStr
	})
	.then(resolve => resolve.text())
	.then(result => console.log(result))
	.catch(err => console.log(err))
}

function addCenterInfoFnc(args={}){
	let param= 'id=' + args.id + '&cn=' + args.centerName + '&pn=' + args.phoneNumber + '&add=' + args.address;
	fetch('addCenterInfo.do', {
		method: 'post',
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		body: param
	})
	.then(resolve => resolve.text())
	.then(result => console.log(result))
	.catch(err => console.log(err))
}
	
</script>