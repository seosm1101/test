<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h3>회원정보 등록</h3>
<form action="inserMember.do" method="post">
	<table class="table">
		<tr>
			<td>회원아이디</td>
			<td><input type="text" name="memberId"></td>
		</tr>
		<tr>
			<td>비밀번호</td>
			<td><input type="password" name="memberPw"></td>
		</tr>
		<tr>
			<td>이름</td>
			<td><input type="text" name="memberName"></td>
		</tr>
		<tr>
			<td>연락처</td>
			<td><input type="text" name="memberPhone"></td>
		</tr>
		<tr>
			<td>주소</td>
			<td><input type="text" name="memberAddr"></td>
		</tr>
		<c:choose>
			<c:when test="${grade=='Admin' }">
				<tr>
					<td>등급</td>
					<td><input type="text" name="memberGrade"></td>
				</tr>
			</c:when>
			<c:otherwise>
			</c:otherwise>
		</c:choose>
		<tr>
			<td>
				<input type="submit" value="등록">
				<input type="reset" value="취소">
			</td>
		</tr>
	</table>
</form>
